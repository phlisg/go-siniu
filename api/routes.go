package api

import (
	"net/http"
	"siniu/src/Models"
	"encoding/json"
	"strings"
	"log"
	"siniu/src"
	"io/ioutil"
)

func Setup(app *App) {
	s := src.Init{}
	s.New()

	js, _ := json.Marshal(&s)
	app.W.Write([]byte(js))
}

func NotFound(app *App) {
	app.W.WriteHeader(http.StatusNotFound)
	app.W.Write([]byte("404 - Not found"))
}

func GetHelp(app *App) {
	helpFile, notFound := ioutil.ReadFile("./A LIRE - INSTALLATION - AIDE.html")

	if notFound != nil {
		var err error
		helpFile, err = ioutil.ReadFile("../A LIRE - INSTALLATION - AIDE.html")

		if err != nil {
			log.Fatalln("Did not find the help file")
			NotFound(app)

			return
		}
	}
	app.W.Write(helpFile)
}

/*
* Profiles
*/

func GetProfiles(app *App) {
	var profiles Models.Profiles
	for _, profile := range profiles.List() {
		app.W.Write([]byte(profile))
	}
}

func GetKeyNames(app *App) {
	var profile Models.Profile
	p, _ := profile.GetKeyNames()
	app.W.Write([]byte(p))
}

func PostProfiles(app *App) {
	var profile Models.Profile
	profile.Create(app.R)
	js, _ := profile.Encode()
	profile.SaveToFile(js)
	app.W.Write([]byte(js))
}

func EditProfiles(app *App) {
	var profile Models.Profile
	profile.Edit(app.R)
	js, _ := profile.Encode()
	profile.SaveToFile(js)
}

func DeleteProfiles(app *App) {
	var profile Models.Profile
	var signature Models.Signature
	pro, err := profile.Delete(app.R)
	if err != nil {
		log.Fatal(err)
	}

	sig, err := signature.Delete(app.R)
	if err != nil {
		log.Fatal(err)
	}

	ret := append(pro, sig...)
	app.W.Write([]byte(ret))
}

/*
* Signatures
*/

func GetSignatures(app *App) {
	var signatures Models.Signatures
	s, _ := signatures.List()

	// REST: ask for a direct profile (api/signatures/jasonhull
	//if app.Path[len(app.Path)-1] != "signatures" {
	//	sig := app.Path[len(app.Path)-1:] // last member of the PATH -> array
	//	js, _ := json.Marshal(s[sig[0]])
	//	app.W.Write(js)
	//	return
	//}
	js, _ := json.Marshal(s)

	app.W.Write(js)
	return
}

func GetSignatureTemplates(app *App) {
	var signatures Models.Signatures
	j := signatures.GetTemplates()

	// REST: asked for a direct template
	if app.Path[len(app.Path)-1] != "templates" { // api/signatures/templates/lausannetourisme.js
		tpl := app.Path[len(app.Path)-1:] // we get the requested file from the path
		s := strings.Split(tpl[0], ".")   // we need to get lausannetourisme without ".js" to get the correct file from "j"
		// [lausannetourisme] [js]
		app.W.Write([]byte(j[s[0]][s[1]]))
		return
	}

	// displaying all templates as json
	v, err := json.Marshal(j)

	if err != nil {
		log.Fatal(err)
	}

	app.W.Write([]byte(v))
}

func CreateSignature(app *App) {
	var signature Models.Signature
	if err := signature.Create(app.R); err == nil {
		// success
		resp, _ := json.Marshal(map[string]string{
			"Success": "Signature created successfully.",
		})

		app.W.Write([]byte(resp))
	} else {
		log.Print(err)
		resp, _ := json.Marshal(map[string]string{
			"Error": "Failed to create signature: " + err.Error(),
		})

		app.W.Write([]byte(resp))
	}
}

func IsInstalledSignature(app *App) {
	var signature Models.Signature
	app.R.ParseForm()

	filename := app.R.Form.Get("file")
	jsn, _ := json.Marshal(map[string]interface{}{"installed": signature.IsInstalled(filename)})

	app.W.Write(jsn)
}

func InstallSignature(app *App) {
	var signature Models.Signature
	app.R.ParseForm()

	filename := app.R.Form.Get("file")
	jsn, _ := json.Marshal(map[string]interface{}{"installed": signature.Install(filename)})

	app.W.Write(jsn)

}

func DeleteSignature(app *App) {
	var signature Models.Signature

	if sig, err := signature.Delete(app.R); err == nil {
		app.W.Write([]byte(sig))
		return
	}
}
