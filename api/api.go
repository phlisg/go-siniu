package api

import (
	"net/http"
	"strings"
	"log"
)

type App struct {
	W    http.ResponseWriter
	R    *http.Request
	Path []string
}

type Routes map[string]map[string]map[string]func(app *App)

var routes = Routes{
	"setup": {
		"GET": {
			"default": Setup,
		},
	},
	"error": {
		"GET": {
			"notfound": NotFound,
		},
	},
	"help": {
		"GET": {
			"default": GetHelp,
		},
	},
	"profiles": {
		"GET": {
			"default":     GetProfiles,
			"getkeynames": GetKeyNames,
		},
		"POST": {
			"default": PostProfiles,
			"edit":    EditProfiles,
			"delete":  DeleteProfiles,
		},
	},
	"signatures": { // api/signatures/templates
		"GET": {
			"default":     GetSignatures,
			"templates":   GetSignatureTemplates,
			"isinstalled": IsInstalledSignature,
		},
		"POST": {
			"create":  CreateSignature,
			"install": InstallSignature,
			"delete":  DeleteSignature,
		},
	},
}

func (app *App) Get(path string) {
	app.Path = strings.Split(path, "/")

	if len(app.Path) > 1 && app.Path[1] == "" {
		app.Path[1] = "default"
	}

	log.Printf("Path: %v | Method: %v", path, app.R.Method)

	// if value, isset := ...
	if _, ok := routes[app.Path[0]]; !ok {
		r := routes["error"]["GET"]["notfound"]
		r(app)
		return
	}

	r := routes[app.Path[0]][app.R.Method][app.Path[1]]
	r(app)
}
