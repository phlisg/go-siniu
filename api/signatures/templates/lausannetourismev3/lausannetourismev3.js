/**
 * Self-executing "template-intelligence" file
 * Acts as a template descriptor and manipulator
 *
 * Three methods are compulsory: preview(), renderForm() and render()
 * RenderForm():
 * - Form that allows the construction of one signature, define here all the necessary fields.
 * Each field needs to have an event that fires the Preview() method, in order to refresh the signature
 *
 * Preview():
 * - Acts as a event handler, when an input has been updated, refreshes the signature via render()
 *
 * Render():
 * - Render needs a HTML element with .preview class in order to display itself.
 * Rebuilds the whole signature with values from the form, via helper methods
 * returning blissjs compatible markup into the renderer (this.getFlags(), this.getSocial()...)
 *
 * In the constructor, define properties such as social networks. Define one property "signature" which will hold
 * the current signature's data.
 * */
class Lausannetourismev3 {
    constructor() {

        this.signature = {
            note: "",
        };

        this.profiles = {};

        this.renderForm().getProfiles($('.form .profiles select'));
    }

    settings() {
        return {
            formSubmit: "/api/signatures/create"
        }
    }

    preview() {
        let networks = $$('.configuration .networks input');
        let note = $('.configuration .note input');
        let banner = $('.configuration .banner input');

        if (networks.length > 0) {
            networks.forEach((network) => {
                let name = network.name;
                if (network.checked && this.signature.social[name] === undefined) {
                    this.signature.social[name] = {content: [this.social(name), this.spacer()]};
                }
                if (!network.checked && this.signature.social[name] !== undefined) {
                    delete (this.signature.social[name]);
                }
            });
        }

        this.signature.note = note.value;

        this.render();
    }

    renderForm() {
        let form = document.querySelector('.form');
        let profiles = form.querySelector('.profiles');
        let configuration = form.querySelector('.configuration');
        let div = document.createElement('div');
        let label = document.createElement('label');
        let input = document.createElement('input');

        let noteLabel = $.create('label', {
            className: "label",
            textContent: "Note:"
        });
        let noteDiv = $.create('div', {
            className: "note"
        });

        let profileSelect = profiles._.contents({
            className: "field profiles",
            contents: [
                {
                    tag: "label",
                    className: "label",
                    textContent: "Sélectionnez un de vos profils"
                },
                {
                    tag: "div",
                    className: "control",
                    contents: [
                        {
                            tag: "div",
                            className: "select is-dark",
                            contents: {
                                tag: "select"
                            }
                        },
                        {
                            tag: "a",
                            href: "#profiles",
                            className: "button is-success is-outlined",
                            textContent: "Créer un profil"
                        }
                    ]
                }
            ]
        });

        let config = configuration._.contents({
            className: "field configuration columns is-multiline",
            contents: [
                $.create('div', {
                    className: "column is-one-third",
                    contents: [noteLabel, noteDiv],
                }),
            ]
        });

        div.classList.add('control');
        label.classList.add('checkbox', 'button', 'is-dark', 'is-outlined');
        label.style.cssText = "text-transform:capitalize;margin:5px;";
        input.type = "checkbox";

        /**
         * Note
         * */
        let n = $.create('div', {
            className: "field",
            contents: {
                tag: "label",
                className: "control",
                oninput: this.preview.bind(this), // "this" in preview changes to input :/
                contents: {
                    tag: "input",
                    className: "input",
                    type: "text",
                    placeholder: "Absent les vendredi"
                }
            }
        });

        try {
            noteDiv.insertAdjacentElement('beforeend', n);
        } catch (e) {
            console.warn(e);
        }

        form.onsubmit = (e) => {
            e.preventDefault(); // we don't want the form to submit, but rather to control what we send
            this.render();
            let parse = new Parse();

            // need to send a json file with obtained values
            let data = {
                note: n.value,
                profile: this.profiles[this.profiles.current],
            };

            /**
             * fixme: not very pretty thingy to do
             * Adding visual spacing as carriage return:
             * */
            let spacer = $.create(this.span({display: "inline"}, {contents: this.spacer()}));
            let template = spacer.outerHTML + this.template.outerHTML;

            aja()
                .url(this.settings().formSubmit)
                .method('post')
                .type('json')
                .data({
                    values: JSON.stringify(data),
                    html: parse.encode(template),
                    profile: (this.profiles[this.profiles.current].FirstName.toLowerCase() + this.profiles[this.profiles.current].LastName.toLowerCase()).replace(/[-]/ig, ""),
                    template: 'lausannetourismev2',
                    identifier: this.profiles[this.profiles.current].Identifier
                })
                .on('success', (m) => {
                    admin.updateMessage(m[Object.keys(m)[0]]);
                    location.href = "#signatures";
                    cache.setItem('signatureCreated', "true");
                })
                .go();
        };

        return this;
    }

    getProfiles(select) {
        select.classList.add('is-loading');

        aja()
            .url("/api/profiles/")
            .method("get")
            .type('json')
            .on('success', (json) => {
                if (json.length < 1) return;

                json.forEach((j) => {
                    let option = document.createElement('option');
                    option.textContent = `${j.Identifier} - ${j.FirstName} ${j.LastName}`;
                    option.value = j.Id;
                    select.appendChild(option);
                    this.profiles[j.Id] = j;
                });

                select.onchange = () => {
                    this.profiles.current = select[select.selectedIndex].value;
                    this.render();
                };

                this.profiles.current = select[select.selectedIndex].value;
                select.classList.remove('is-loading');
                this.render();
            })
            .go();

        return this;
    };


    social(name) {
        let link = "https://apps.lausanne-tourisme.ch/signatures/asset/";
        if (name === "none") return;
        return {
            tag: 'a',
            href: this.networks[name].link,
            style: {textDecoration: "none"},
            contents: this.img({
                src: `${link}${this.networks[name].name}.png`,
                height: 16,
                width: 16,
                style: {marginBottom: "2px", border: 'none', display: 'inline'}
            })
        }
    }

    getSocial() {
        if (Object.keys(this.signature.social).length === 0) return this.a();
        let ret = [];

        for (let network in this.signature.social) {
            ret = ret.concat([...this.signature.social[network].content]);
        }

        return ret;
    }

    br() {
        return {
            tag: "br",
        };
    }

    a(options) {
        let ret = {
            tag: "a",
            style: {color: "rgb(50,50,50)", textDecoration: "none", display: "inline"}
        };

        Object.assign(ret, options);

        return ret;
    }

    span(style, options) {
        let ret = {
            tag: "span",
            style: style || {},
        };

        Object.assign(ret, options);
        return ret;
    }

    spacer(width = 12, height = 0) {
        return {
            tag: "img",
            src: "https://apps.lausanne-tourisme.ch/signatures/asset/spacer.png",
            width: width,
            height: height,
        };
    }

    img(options) {
        let ret = {
            tag: "img",
        };

        Object.assign(ret, options);

        return ret;
    }

    /**
     * Builds the virtual signature, updates the fields, and refreshes the view
     * */
    render() {
        let signature = $.create('table', {
            width: "550",
            cellspacing: 0,
            cellpadding: 0,
            border: 0,
            style: {
                borderCollapse: "separate",
                tableLayout: "fixed"
            },
            contents: {
                tag: "tbody",
                contents: [
                    {
                        tag: "tr",
                        contents: [
                            {
                                tag: "td",
                                align: "left",
                                width: 90,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: [
                                    {
                                        tag: "a",
                                        href: "https://www.lausanne-tourisme.ch",
                                        contents: {
                                            tag: "img",
                                            src: "https://apps.lausanne-tourisme.ch/signatures/asset/LT-Logo.png",
                                            width: 80,
                                            height: 105,
                                        }
                                    }
                                ]
                            },
                            {
                                tag: "td",
                                align: "left",
                                width: 10,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: this.spacer(),
                            },
                            { // MAIN INFO
                                tag: "td",
                                align: "left",
                                width: 246,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: [
                                    {
                                        tag: 'p',
                                        style: {
                                            fontFamily: "Helvetica, Arial, sans-serif",
                                            fontSize: "12px",
                                            lineHeight: "14px",
                                            color: "rgb(33, 33, 33)",
                                        },
                                        contents: [
                                            this.span({
                                                fontWeight: "bold",
                                                color: "rgb(33, 33, 33)",
                                                display: "inline"
                                            }, {
                                                textContent: this.profiles[this.profiles.current].FirstName.toUpperCase() + " "
                                            }),
                                            this.span({
                                                fontWeight: "bold",
                                                color: "rgb(33, 33, 33)",
                                                display: "inline"
                                            }, {
                                                attributes: {"data-field": "lastName"},
                                                textContent: this.profiles[this.profiles.current].LastName.toUpperCase()
                                            }),
                                            this.span({display: 'inline'}, {contents: this.br()}),
                                            this.span({color: "rgb(33, 33, 33)", display: "inline"}, {
                                                attributes: {"data-field": "title"},
                                                textContent: this.profiles[this.profiles.current].Title.toUpperCase()
                                            }),
                                            this.span({display: 'inline'}, {contents: this.br()}),
                                            this.span({color: "rgb(33, 33, 33)", display: "inline"}, {
                                                attributes: {"data-field": "department"},
                                                textContent: this.profiles[this.profiles.current].Department.toUpperCase()
                                            }),
                                            this.span({display: 'inline'}, {contents: this.br()}),
                                            this.span({display: 'inline'}, {contents: this.br()}),
                                            this.span({
                                                fontSize: "12px", lineHeight: "14px",
                                                fontFamily: "Helvetica, Arial, sans-serif"
                                            }, {
                                                innerHTML: "T&nbsp;&nbsp;&nbsp;"
                                            }),
                                            this.a({
                                                attributes: {"data-field": "phone"},
                                                style: {
                                                    color: "rgb(50,50,50)", textDecoration: "none", display: "inline",
                                                    fontSize: "12px", lineHeight: "14px",
                                                    fontFamily: "Helvetica, Arial, sans-serif",
                                                },
                                                href: "tel:" + this.profiles[this.profiles.current].Phones[0],
                                                textContent: swissPhoneFormat(this.profiles[this.profiles.current].Phones[0]).join(' ')
                                            }),
                                            this.span({display: "inline"}, this.br()),
                                            this.span({
                                                fontSize: "12px", lineHeight: "14px",
                                                fontFamily: "Helvetica, Arial, sans-serif",
                                            }, {
                                                innerHTML: "E&nbsp;&nbsp;&nbsp;",
                                            }),
                                            this.a({
                                                attributes: {'data-field': 'email'},
                                                href: "mailto:" + this.profiles[this.profiles.current].Email,
                                                style: {
                                                    color: "rgb(50,50,50)",
                                                    textDecoration: "none",
                                                    display: "inline",
                                                    fontSize: "12px",
                                                    fontFamily: "Helvetica, Arial, sans-serif",
                                                },
                                                textContent: this.profiles[this.profiles.current].Email,
                                            }),
                                            this.span({display: "inline"}, {contents: this.br()}),
                                            this.span({display: "inline"}, {contents: this.br()}),
                                            this.a({
                                                href: "https://www.facebook.com/LausanneCapitaleOlympique",
                                                style: {textDecoration: 'none'},
                                                contents: this.img({
                                                    tag: 'img',
                                                    style: {
                                                        marginTop: "5px",
                                                    },
                                                    src: "https://apps.lausanne-tourisme.ch/signatures/asset/facebook-black.png",
                                                    height: 15,
                                                    width: 8,
                                                    border: 0
                                                })
                                            }),
                                            this.img(this.spacer(5)),
                                            this.a({
                                                href: "https://www.twitter.com/LausanneCO",
                                                style: {textDecoration: 'none'},
                                                contents: this.img({
                                                    tag: "img",
                                                    style: {
                                                        marginTop: "5px",
                                                    },
                                                    src: "https://apps.lausanne-tourisme.ch/signatures/asset/twitter-black.png",
                                                    height: 13,
                                                    width: 14,
                                                    border: 0
                                                })
                                            }),
                                            this.img(this.spacer(5)),
                                            this.a({
                                                attributes: {'data-field': 'email'},
                                                href: "https://www.linkedin.com/company/lausanne-tourisme-&-convention-bureau/",
                                                style: {
                                                    color: "rgb(50,50,50)", textDecoration: "none", display: "inline"
                                                },
                                                contents: {
                                                    tag: "img",
                                                    style: {
                                                        marginTop: "5px",
                                                    },
                                                    width: 14,
                                                    height: 14,
                                                    src: "https://apps.lausanne-tourisme.ch/signatures/asset/ln-black-14px.png"
                                                },
                                            }),
                                            this.span({display: "inline"}, {contents: this.br()}),
                                        ]
                                    }, // end of p
                                    {
                                        tag: "p",
                                        style: {
                                            fontFamily: "Helvetica, Arial, sans-serif",
                                            fontSize: "12px",
                                            lineHeight: "14px",
                                            marginBottom: "15px"
                                        },
                                        contents: [
                                            this.a({
                                                href: "https://www.google.ch/maps/place/Avenue+de+Rhodanie+2,+1007+Lausanne/@46.5079627,6.62258,17z/",
                                                innerHTML: "Av. de Rhodanie 2 - CP 975 <br>1001 Lausanne"
                                            }),
                                        ]
                                    }, // end of p
                                ], // end of div
                            }, // end of td
                            {
                                tag: "td",
                                align: "left",
                                width: 10,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: this.spacer(),
                            },
                            { // BLACK BORDER LINE + SPACER
                                tag: "td",
                                //style: {borderLeftWidth: "1px", borderLeftStyle: "solid", borderLeftColor: "rgb(0, 119, 190)"},
                                attributes: {"style": 'border-left-width: 1px; border-left-style: solid; border-left-color: rgb(0,0,0)'},
                                align: "left",
                                width: 20,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: this.spacer()
                            }, // end of td
                            { // LOGO + "FLAG"
                                tag: "td",
                                align: "left",
                                width: 152,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: [
                                    this.span({
                                            marginBottom: "10px",
                                            marginRight: "8px",
                                        }, this.a({
                                            href: "https://www.the-lausanner.ch/",
                                            style: {
                                                textDecoration: 'none',
                                                fontFamily: "Helvetica, Arial, sans-serif",
                                                fontSize: "10px",
                                                lineHeight: "12px"
                                            },
                                            contents: this.img({
                                                src: "https://apps.lausanne-tourisme.ch/signatures/asset/lausanner-small.png",
                                                height: 49,
                                                width: 130,
                                                border: 0
                                            })
                                        })
                                    ), // end of p
                                    this.spacer(1, 6),
                                    {
                                        tag: "p",
                                        style: {
                                            marginBottom: "10px",
                                            marginRight: "8px",
                                        },
                                        contents: [
                                            this.a({
                                                href: "https://www.instagram.com/thelausanner",
                                                style: {textDecoration: 'none'},
                                                contents: this.img({
                                                    src: "https://apps.lausanne-tourisme.ch/signatures/asset/instagram-black-lausanner.png",
                                                    height: 14,
                                                    width: 102,
                                                    border: 0
                                                })
                                            }),
                                        ],
                                    },
                                    // {
                                    //     tag: "p",
                                    //     style: {
                                    //         marginTop: "6px",
                                    //         marginRight: "8px",
                                    //         fontFamily: "Helvetica, Arial, sans-serif",
                                    //         fontSize: "12px",
                                    //         lineHeight: "12px",
                                    //     },
                                    //     contents: [
                                    //         this.a({
                                    //             href: "https://www.instagram.com/thelausanner",
                                    //             style: {
                                    //                 textDecoration: "none",
                                    //                 marginTop: "3px",
                                    //             },
                                    //             contents: this.img({
                                    //                 src: "https://apps.lausanne-tourisme.ch/signatures/asset/instagram-black.png",
                                    //                 height: 14,
                                    //                 width: 14,
                                    //                 border: 0
                                    //             })
                                    //         }),
                                    //     ]
                                    // }, // end of p
                                    {
                                        tag: "p",
                                        style: {
                                            marginTop: "10px",
                                            marginBottom: "10px",
                                            marginRight: "8px",
                                            fontFamily: "Helvetica, Arial, sans-serif",
                                            fontSize: "12px",
                                            lineHeight: "14px",
                                            display: "block"
                                        },
                                        textContent: "#MyLausanne #TheLausanner",
                                        contents: [
                                            this.br(),
                                            this.a({
                                                href: "https://www.lausanne-tourisme.ch/fr/the-lausanner/",
                                                textContent: "thelausanner.ch",
                                                style: {
                                                    fontWeight: "bold",
                                                    textDecoration: "none",
                                                    color: "rgb(0,0,0)"
                                                }
                                            })
                                        ]
                                    }, // end of p
                                ]
                            }, // end of td
                            {
                                tag: "td",
                                align: "left",
                                width: 10,
                                valign: "top",
                                nowrap: "nowrap",
                                contents: this.spacer(),
                            },
                        ]
                    }, // end of tr
                    { // NOTE
                        tag: 'tr',
                        attributes: {'data-field': 'note'},
                        contents: {
                            tag: "td",
                            colspan: 3,
                            contents: {
                                tag: "p",
                                style: {
                                    fontFamily: "Helvetica, Arial, sans-serif",
                                    color: "#212121",
                                    fontSize: "9px",
                                    lineHeight: "12px",
                                    marginTop: "10px"
                                },
                                textContent: this.signature.note
                            }
                        }
                    }, // end of tr
                ]
            } // end of tbody
        });

        let preview = document.querySelector('.preview');
        preview.innerHTML = '';
        preview.appendChild($.create(this.span({display: "inline"}, {contents: this.spacer()})));
        preview.appendChild(signature);

        this.template = signature;

        return this;
    }
}

// on call, register the function
// fixme: should be called when signature template is selected
if (window.templates) {
    if (!window.templates["lausannetourismev3"]) window.templates['lausannetourismev3'] = Lausannetourismev3;
} else if (!window.templates) {
    window.templates = {lausannetourismev2: Lausannetourismev3};
}