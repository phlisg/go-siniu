const gulp = require('gulp');
const concat = require('gulp-concat');
const watch = require('gulp-watch');

const paths = {
	js : {
		src: "./static/js/src/*.js",
		output: "./static/js/min"
	}
};

gulp.task('default', function() {
	return gulp.src(paths.js.src)
		//.pipe(watch(paths.js.src))
		.pipe(concat('app.js'))
		.pipe(gulp.dest(paths.js.output));
});