package main

import (
	"bitbucket.org/phlisg/viewr/src/utils"
	"bytes"
	"context"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"siniu/api"
	"strings"
	"text/template"
)

var (
	localhost = "http://localhost"
	port      = ":80"
	admin     = localhost + port
	srv       = &http.Server{}
	isDev     = isDevFlag()
)

func main() {
	// If a flag is set on the executable, do special stuff
	if isDev {
		localhost = "http://phlisg.go"
		admin = localhost + port + "/?admin"
	}
	// Creating base Handlers and server
	srv = &http.Server{Addr: port, Handler: nil}
	defer srv.ListenAndServe()
	http.HandleFunc("/", app)

	// Creating a file server to serve CSS and JS files
	fs := http.Dir("./static/")
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(fs)))

	// Finally, writing some gibberish and opening the browser
	var buffer bytes.Buffer
	buffer.WriteString("Listening on " + localhost + port + "...")
	log.Println(buffer.String())
	go utils.Open(admin)
}

func app(w http.ResponseWriter, r *http.Request) {

	path := r.URL.Path
	app := api.App{w, r, []string{}}

	// client view
	if path == "/" {
		ExecTemplates(w)
		return
	}

	// api view
	if strings.Index(path, "/api") > -1 {
		app.Get(strings.Replace(path, "/api/", "", 1))
		return
	}

	// coming from unsupported browser
	if strings.Index(path, "chrome") > -1 {
		Run("chrome")
		http.Redirect(app.W, app.R, localhost+port, 301)
		return
	}

	if strings.Index(path, "firefox") > -1 {
		Run("firefox")
		http.Redirect(app.W, app.R, localhost+port, 301)
		return
	}

	if strings.Index(path, "shutdown") > -1 && !isDev {
		if err := srv.Shutdown(context.Background()); err != nil {
			log.Fatalf("Error: %v", err)
		} else {
			log.Println("Server stopped.")
		}
		return
	}

	Print(path, app.R.Method)
	app.W.WriteHeader(http.StatusNotFound)
	app.W.Write([]byte("404 - Not found"))
}

func ExecTemplates(w io.Writer) {
	var templates *template.Template
	templates, err := template.ParseFiles(ListTemplates()...) // Parses all .tmpl files in templates folder

	if err != nil {
		log.Fatal(err)
	}

	s1 := templates.Lookup("layout.html.tmpl")
	s1.ExecuteTemplate(w, "layout", nil)
}

func ListTemplates() []string {
	var files []string
	f, err := ioutil.ReadDir("./src/Templates")

	if err != nil {
		log.Fatal(err)
	}

	for _, file := range f {
		filename := file.Name()

		if strings.HasSuffix(filename, ".tmpl") {
			files = append(files, "./src/Templates/"+filename)
		}
	}

	return files
}

func Print(w ...interface{}) {
	log.Printf("%v", w)
}

func isDevFlag() bool {
	isDev := flag.Bool("dev", false, "Dev mode: runs a few things separately.") // we have pointer to those values
	flag.Parse()

	return *isDev
}

func Run(app string) {
	cmd := exec.Command("cmd", "/c", "start", localhost+port, app)
	err := cmd.Start()
	if (err != nil) {
		Print(err)
	}
}
