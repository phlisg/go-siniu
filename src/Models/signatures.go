package Models

import (
	"os/user"
	"strings"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
	"encoding/json"
	"bitbucket.org/phlisg/viewr/src/utils"
)

type Templates map[string]map[string]string
type T map[string]string

type Signatures struct {
	Signature map[string]Signature
}

type Signature struct {
	Id         string
	Identifier string
	Profile    string // id of profile, as profile implements a search method
	Html       string // html of the template, to be encoded in a file
	Values     string // json values to store them in a json file to be able to retrieve/edit the signature
	Template   string
}

var signatures = NewSignatures()

func init() {
	signatures.Setup()
}

func GetUserDir() string {
	cu, _ := user.Current()
	u := strings.Split(cu.Username, "\\")[1] // separates group domain from user

	return "c:/Users/" + u + "/AppData/Roaming/Microsoft/Signatures/"
}

func NewSignatures() Signatures {
	return Signatures{}
}

func (signatures *Signatures) Setup() (*Signatures) {
	signatures.Signature = make(map[string]Signature)
	_, signs := signatures.List()

	signatures.Signature = signs
	log.Printf("Signatures: %v", signatures)

	return signatures
}

func (signatures *Signatures) ListUserDir() (list []string) {
	readDir, err := ioutil.ReadDir(GetUserDir())

	if err != nil {
		log.Fatal(err)
	}

	for _, file := range readDir {
		if !file.IsDir() {
			list = append(list, file.Name())
		}
	}

	return list
}

func (signatures *Signatures) List() (s map[string]map[string]map[string][]map[string]string, signs map[string]Signature) { // beautiful
	exports := "./api/signatures/exports/"
	templates, err := ioutil.ReadDir(exports)

	s = map[string]map[string]map[string][]map[string]string{}
	signs = make(map[string]Signature)

	if err != nil {
		log.Fatal(err)
	}

	/**
	Behold: massive loop :(
	*/
	for _, template := range templates {
		if template.IsDir() {
			profiles, err := ioutil.ReadDir(exports + template.Name())
			s[template.Name()] = map[string]map[string][]map[string]string{}
			if err != nil {
				log.Fatal(err)
			}

			for _, profile := range profiles {
				if profile.IsDir() {
					sigs, err := ioutil.ReadDir(exports + template.Name() + "/" + profile.Name())
					if err != nil {
						log.Fatal(err)
					}

					/* once we're in a profile, we want to build a map containing
					 * as key the file extension, and as value the file contents.
					 * We also add in the ID of the file.
					 */
					s[template.Name()][profile.Name()] = map[string][]map[string]string{}
					var identifier, id, path string

					for _, sig := range sigs {
						if !sig.IsDir() {
							data := map[string]string{} // setting or resetting data

							// path without extension so we can open both json/html file at once
							path = exports + template.Name() + "/" + profile.Name() + "/" + strings.Split(sig.Name(), ".")[0]

							// we get the 2nd member of file name
							identifier = strings.Split(sig.Name(), "-")[1]

							// we get the 3rd member of file name, then strip out extension
							id = strings.Split(strings.Split(sig.Name(), "-")[2], ".")[0]

							// reading both files at once
							jsn, err := ioutil.ReadFile(path + ".json")
							htm, err := ioutil.ReadFile(path + ".htm")

							if err != nil {
								log.Fatal(err)
							}

							signature := Signature{
								Id:         id,
								Identifier: identifier,
								Html:       string(htm),
								Profile:    string(jsn),
								Template:   template.Name(),
							}

							data["htm"] = string(htm)
							data["json"] = string(jsn)
							data["id"] = id

							signs[id] = signature
							/*
							Since there are 2 files for one signatures, we need to skip one append
							*/
							if strings.Index(sig.Name(), ".json") == -1 {
								s[template.Name()][profile.Name()][identifier] = append(s[template.Name()][profile.Name()][identifier], data)
							}
						}
					}
				}
			}
		}
	}

	return s, signs
}

func (signatures *Signatures) getRemoteList() () {
	// we get local signatures first
	//_, localList := signatures.List()

	// we get remote files
	var remoteList map[string]string

	dir, err := ioutil.ReadDir(GetUserDir())

	if err != nil {
		log.Fatal(err)
	}

	for _, signs := range dir {
		if !signs.IsDir() {
			split := strings.Split(signs.Name(), "-")

			if len(split) > 2 {
				id := split[2]
				remoteList[id] = signs.Name()
			}
		}
	}
}

func (signatures *Signatures) GetTemplates() (list Templates) {
	dir := "./api/signatures/templates/"
	readDir, err := ioutil.ReadDir(dir)

	if err != nil {
		log.Fatal(err)
	}

	list = map[string]map[string]string{}

	for _, folder := range readDir {
		if folder.IsDir() {
			f, err := ioutil.ReadDir(dir + folder.Name())
			if err != nil {
				log.Fatal(err)
			}
			list[folder.Name()] = T{}
			for _, file := range f {
				if !file.IsDir() {
					ext := strings.Split(file.Name(), ".")[1]
					content, _ := ioutil.ReadFile(dir + folder.Name() + "/" + file.Name())

					list[folder.Name()][ext] = string(content)
				}
			}
		}
	}

	return list
}

func (signatures *Signatures) Append(s Signature) *Signatures {
	signatures.Signature = make(map[string]Signature)
	signatures.Signature[s.Id] = s

	return signatures
}

func (signature Signature) Create(r *http.Request) (err error) {
	r.ParseForm()
	d := time.Now().Format("20060102150405")

	signature = Signature{
		Id:         d,
		Identifier: r.Form.Get("identifier"),
		Profile:    r.Form.Get("profile"),
		Html:       r.Form.Get("html"),
		Values:     r.Form.Get("values"),
		Template:   r.Form.Get("template"),
	}

	exports := "./api/signatures/exports/" + signature.Template + "/"
	_, err = os.Stat(exports)

	if err != nil {
		// folder does not exist
		log.Print(err)
		os.Mkdir(exports, 0664)
		err = nil
	}

	exports += signature.Profile + "/"
	_, err = os.Stat(exports)

	if err != nil {
		// folder does not exist
		log.Print(err)
		os.Mkdir(exports, 0664)
		err = nil
	}

	ioutil.WriteFile(exports+signature.Profile+"-"+signature.Identifier+"-"+d+".htm", []byte(signature.Html), 0755)
	ioutil.WriteFile(exports+signature.Profile+"-"+signature.Identifier+"-"+d+".json", []byte(signature.Values), 0755)

	signatures.Append(signature)
	log.Printf("Signatures: %v", signatures)

	return err
}

func (signature *Signature) IsInstalled(filename string) (exists bool) {
	currentUserDir := GetUserDir()

	if strings.Index(filename, ".htm") > -1 {
		if exists, _ = utils.Exists(currentUserDir + filename); exists {
			return exists // true
		}
	} else {
		if exists, _ = utils.Exists(currentUserDir + filename + ".htm"); exists {
			return exists // true
		}
	}

	return exists // false
}

func (signature *Signature) Install(filename string) bool {
	id := strings.Split(filename, "-")[2]

	if signature.IsInstalled(filename) == true {
		// file is already installed, let's remove it
		return signature.Uninstall(filename)
	}

	if _, exists := signatures.Signature[id]; !exists {
		signatures.Setup()
	}

	if strings.Index(filename, ".htm") == -1 {
		filename += ".htm"
	}

	file, err := os.Create(GetUserDir() + filename)
	defer file.Close()

	if err != nil {
		log.Fatal(err)
	}

	file.Write([]byte(signatures.Signature[id].Html))
	file.Chmod(0755)

	return signature.IsInstalled(filename)
}

/**
Uninstall removes the remote file
*/
func (signature *Signature) Uninstall(filename string) bool {
	if signature.IsInstalled(filename) == true {
		if strings.Index(filename, ".htm") > -1 {
			os.Remove(GetUserDir() + filename)
		} else {
			os.Remove(GetUserDir() + filename + ".htm")
		}
	}

	return signature.IsInstalled(filename)
}

func (signature *Signature) Delete(r *http.Request) ([]byte, error) {
	// We get to the signatures exports folder, then we scan each template and delete found signatures
	signs := &signatures
	dir := "./api/signatures/exports/"
	readDir, _ := ioutil.ReadDir(dir)
	r.ParseForm()
	// if the request comes from AJAX/profiles.html, we use the identifier variable - but this is not enough
	// to properly identify the correct signatures -> need to try to send something else with
	identifier := r.Form.Get("profile") + "-" + r.Form.Get("identifier")
	file := r.Form.Get("file")
	wasDeleted := false

	// if it comes from signatures.html, we use the filename directly, as it's available.
	if file != "" {
		identifier = file
	}

	for _, templates := range readDir {
		if templates.IsDir() {
			profiles, _ := ioutil.ReadDir(dir + templates.Name()) // ./exports/lausannetourisme
			for _, profile := range profiles { // ./exports/lausannetourisme/firstnamelastname
				signatures, _ := ioutil.ReadDir(dir + templates.Name() + "/" + profile.Name())
				for _, sign := range signatures {
					if strings.Index(sign.Name(), identifier) > -1 {
						//err := os.RemoveAll(dir + templates.Name() + "/" + profile.Name())
						err := os.Remove(dir + templates.Name() + "/" + profile.Name() + "/" + sign.Name())
						id := strings.Split(sign.Name(), "-")[2]

						signature.Uninstall(sign.Name())

						delete(signs.Signature, id)

						if err != nil {
							log.Fatalf("[DELETE]: %v", err)
						}
						wasDeleted = true
					}
				}
			}
		}
	}

	if wasDeleted {
		return json.Marshal(map[string]interface{}{"identifier": identifier, "status": "deleted"})
	}

	return json.Marshal(map[string]interface{}{"identifier": identifier, "status": "Not found/Not deleted"})
}
