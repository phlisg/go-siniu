package Models

import (
	"strings"
	"encoding/json"
	"log"
	"net/http"
	"time"
	"io/ioutil"
	"os"
	"reflect"
	"crypto/rand"
)

type Profiles struct {
	Profiles []Profile
}

type Profile struct {
	Id         string
	Identifier string
	FirstName  string
	LastName   string
	Department string
	Title      string
	Email      string
	Phones     []string
}

var dir = "./api/profiles/"
var profiles Profiles

/*
Exports the type Key Names. Not used but left as example.
*/
func (p Profile) GetKeyNames() ([]byte, error) {
	a := &Profile{FirstName: "Prénom", LastName: "Nom", Department: "Département", Title: "Titre", Email: "Email", Phones: []string{"phone0"}}
	keys := reflect.Indirect(reflect.ValueOf(a))

	k := make([]interface{}, keys.NumField())

	for i := 0; i < keys.NumField(); i++ {
		k[i] = map[string]interface{}{"input": keys.Type().Field(i).Name, "name": keys.Field(i).Interface()}
	}

	log.Printf("Reflect: %v", k)

	return json.Marshal(k)
}

func (ps *Profiles) List() ([]string) {
	// FIXME: some serious refactoring should happen to this method
	readDir, err := ioutil.ReadDir(dir)

	if err != nil {
		log.Fatal(err)
	}

	var list []string
	list = append(list, "[")

	nb := len(readDir)
	i := 1 // used to know when we're reaching the last file in the folder to form a correct JSON file

	for _, file := range readDir {
		if !file.IsDir() {
			var p Profile

			file, err := ioutil.ReadFile(dir + file.Name())
			if err != nil {
				log.Fatal(err)
			}

			err = json.Unmarshal(file, &p)
			if err != nil {
				log.Fatal(err)
			}

			ps.Profiles = append(ps.Profiles, p)
			list = append(list, string(file))

			if i < nb {
				list = append(list, ",")
			}

			i += 1
		}
	}

	list = append(list, "]")

	return list
}

func (p *Profile) Find(id string) *Profile {
	ps := Profiles{}
	ps.List()

	for i := 0; i < len(ps.Profiles); i++ {
		if ps.Profiles[i].Id == id {
			*p = ps.Profiles[i]
			break
		}
	}

	return p
}

/**
* @method Profile
* From a form, parses fields and assigns them to a Profiles, then appends new profile to all profiles.
*/
func (p *Profile) Create(r *http.Request) *Profile {
	if err := r.ParseForm(); err != nil {
		log.Fatal(err)
	}

	p.Id = time.Now().Format("20060102150405")
	p.Identifier = r.Form.Get("identifier")
	p.FirstName = r.Form.Get("fname")
	p.LastName = r.Form.Get("lname")
	p.Email = r.Form.Get("email")
	p.Department = r.Form.Get("department")
	p.Title = r.Form.Get("title")

	if p.Identifier == "" {
		n := 5
		b := make([]byte, n)
		if _, err := rand.Read(b); err != nil {
			panic(err)
		}
		p.Identifier = string(b)
	}

	for key, data := range r.Form {
		for _, phone := range data {
			if strings.Index(key, "phone") > -1 {
				p.Phones = append(p.Phones, strings.Replace(phone, " ", "", 0))
			}
		}
	}

	profiles.Profiles = append(profiles.Profiles, *p)

	return p
}

func (p *Profile) Edit(r *http.Request) *Profile {
	if err := r.ParseForm(); err != nil {
		log.Fatal(err)
	}

	p.Id = r.Form.Get("id")
	p.Identifier = r.Form.Get("identifier")
	p.FirstName = r.Form.Get("firstname")
	p.LastName = r.Form.Get("lastname")
	p.Email = r.Form.Get("email")
	p.Department = r.Form.Get("department")
	p.Title = r.Form.Get("title")
	p.Phones = strings.Split(r.Form.Get("phones"), ",")

	return p
}

func (p *Profile) Encode() ([]byte, error) {
	js, err := json.Marshal(p)

	return js, err
}

func (p *Profile) SaveToFile(v []byte) {
	filename := strings.ToLower(strings.Replace(p.FirstName+p.LastName,"-", "", -1)) + "-" + strings.ToLower(p.Identifier) + "-" + p.Id + ".json"

	// checking if file doesn't already exist, if it does, we use that one:
	readDir, _ := ioutil.ReadDir("./api/profiles/")

	for _, file := range readDir {
		if !file.IsDir() && strings.Index(file.Name(), p.Id) > -1 {
			filename = file.Name()
		}
	}

	err := ioutil.WriteFile(dir+filename, v, 0755)
	if err != nil {
		log.Fatal(err)
	}
}

func (p *Profile) Delete(r *http.Request) ([]byte, error) {
	if err := r.ParseForm(); err != nil {
		log.Fatal(err)
	}
	requestedId := r.Form.Get("id")

	readDir, err := ioutil.ReadDir(dir)

	if err != nil {
		log.Fatal(err)
	}

	for _, file := range readDir {
		if !file.IsDir() && strings.Index(file.Name(), requestedId) > -1 {
			err := os.Remove(dir + file.Name())
			if err != nil {
				log.Fatalf("[DELETE]: %v", err)
			}

			return json.Marshal(map[string]bool{"id": true})
		}
	}

	return json.Marshal(map[string]bool{"id": false})
}
