package src

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"siniu/src/Models"
	"strings"
)

type Init struct {
	Initiated bool `json:"initiated"`
}

var s Init

// program has launched : check if setup file exists, otherwise create it
func (s *Init) New() {
	setup := s
	setup.getSetup()

	if setup.Initiated == false {
		profiles := Models.Profiles{}
		profiles.List()

		if (strings.Join(profiles.List(), "")) != "[]" {
			setup.Initiated = true
		}
		// need to create a file
		file, err := os.Create("setup.json")
		defer file.Close()

		if err != nil {
			panic(err)
		}
		content, _ := json.Marshal(setup)
		file.Write(content)
		setup.getSetup()

		return
	}

}

func (s *Init) getSetup() {
	file := "./setup.json"

	fileRead, err := ioutil.ReadFile(file)
	if err != nil {
		// file doesn't exist
		log.Print("Setup file not found... creating one!")
		return
	}
	err = json.Unmarshal(fileRead, &s)

	if err != nil {
		log.Println(err)
	}

}
