/**
 * Get Browser: if IE/Edge/Safari, kill the app
 * */
(function () {
	// Opera 8.0+
	var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
	var isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
	var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
		return p.toString() === "[object SafariRemoteNotification]";
	})(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

// Internet Explorer 6-11
	var isIE = /*@cc_on!@*/false || !!document.documentMode;

// Edge 20+
	var isEdge = !!window.StyleMedia && (navigator.appVersion.indexOf('Edge') >= 0);

// Chrome 1+
	var isChrome = !!window.chrome && !!window.chrome.webstore;

// Blink engine detection
	var isBlink = (isChrome || isOpera) && !!window.CSS;
	
	if (isIE || isSafari || isEdge) {
        document.getElementById('loading').style.display = 'none';
        location.href("#!error");
	}
})();
!function(){"use strict";var a=["html","json","jsonp","script"],b=["connect","delete","get","head","options","patch","post","put","trace"],c=function f(){var a={},b={},c={url:function(a){return h.call(this,"url",a,d.string)},sync:function(a){return h.call(this,"sync",a,d.bool)},cache:function(a){return h.call(this,"cache",a,d.bool)},type:function(a){return h.call(this,"type",a,d.type)},header:function(b,c){return a.headers=a.headers||{},d.string(b),"undefined"!=typeof c?(d.string(c),a.headers[b]=c,this):a.headers[b]},auth:function(b,c){return d.string(b),d.string(c),a.auth={user:b,passwd:c},this},timeout:function(a){return h.call(this,"timeout",a,d.positiveInteger)},method:function(a){return h.call(this,"method",a,d.method)},queryString:function(a){return h.call(this,"queryString",a,d.queryString)},data:function(a){return h.call(this,"data",a,d.plainObject)},body:function(a){return h.call(this,"body",a,null,function(a){if("object"==typeof a){if(!(a instanceof FormData)){try{a=JSON.stringify(a)}catch(b){throw new TypeError("Unable to stringify body's content : "+b.name)}this.header("Content-Type","application/json")}}else a+="";return a})},into:function(a){return h.call(this,"into",a,d.selector,function(a){return"string"==typeof a?document.querySelectorAll(a):a instanceof HTMLElement?[a]:void 0})},jsonPaddingName:function(a){return h.call(this,"jsonPaddingName",a,d.string)},jsonPadding:function(a){return h.call(this,"jsonPadding",a,d.func)},on:function(a,c){return"function"==typeof c&&(b[a]=b[a]||[],b[a].push(c)),this},off:function(a){return b[a]=[],this},trigger:function(a,c){var d=this,e=function(a,c){b[a]instanceof Array&&b[a].forEach(function(a){a.call(d,c)})};if("undefined"!=typeof a){a+="";var f=/^([0-9])([0-9x])([0-9x])$/i,g=a.match(f);g&&g.length>3?Object.keys(b).forEach(function(a){var b=a.match(f);!(b&&b.length>3&&g[1]===b[1])||"x"!==b[2]&&g[2]!==b[2]||"x"!==b[3]&&g[3]!==b[3]||e(a,c)}):b[a]&&e(a,c)}return this},go:function(){var b=a.type||(a.into?"html":"json"),c=j();return"function"==typeof g[b]?g[b].call(this,c):void 0}},g={json:function(a){var b=this;g._xhr.call(this,a,function(a){if(a)try{a=JSON.parse(a)}catch(c){return b.trigger("error",c),null}return a})},html:function(b){g._xhr.call(this,b,function(b){return a.into&&a.into.length&&[].forEach.call(a.into,function(a){a.innerHTML=b}),b})},_xhr:function(b,c){var d,e,f,g,h,j=this,k=a.method||"get",l=a.sync!==!0,m=new XMLHttpRequest,n=a.data,o=a.body,p=(a.headers||{},this.header("Content-Type")),q=a.timeout;if(!p&&n&&i()&&(this.header("Content-Type","application/x-www-form-urlencoded;charset=utf-8"),p=this.header("Content-Type")),n&&i())if("string"!=typeof o&&(o=""),p.indexOf("json")>-1)try{o=JSON.stringify(n)}catch(r){throw new TypeError("Unable to stringify body's content : "+r.name)}else{g=p&&p.indexOf("x-www-form-urlencoded")>1;for(d in n)o+=g?encodeURIComponent(d)+"="+encodeURIComponent(n[d])+"&":d+"="+n[d]+"\n\r"}h=[k,b,l],a.auth&&(h.push(a.auth.user),h.push(a.auth.passwd)),m.open.apply(m,h);for(e in a.headers)m.setRequestHeader(e,a.headers[e]);m.onprogress=function(a){a.lengthComputable&&j.trigger("progress",a.loaded/a.total)},m.onload=function(){var a=m.responseText;f&&clearTimeout(f),this.status>=200&&this.status<300&&("function"==typeof c&&(a=c(a)),j.trigger("success",a)),j.trigger(this.status,a),j.trigger("end",a)},m.onerror=function(a){f&&clearTimeout(f),j.trigger("error",a,arguments)},q&&(f=setTimeout(function(){j.trigger("timeout",{type:"timeout",expiredAfter:q},m,arguments),m.abort()},q)),m.send(o)},jsonp:function(b){var c,d=this,g=document.querySelector("head"),h=a.sync!==!0,i=a.jsonPaddingName||"callback",j=a.jsonPadding||"_padd"+(new Date).getTime()+Math.floor(1e4*Math.random()),k={};if(f[j])throw new Error("Padding "+j+" already exists. It must be unique.");/^ajajsonp_/.test(j)||(j="ajajsonp_"+j),window[j]=function(a){d.trigger("success",a),g.removeChild(c),window[j]=void 0},k[i]=j,b=e(b,k),c=document.createElement("script"),c.async=h,c.src=b,c.onerror=function(){d.trigger("error",arguments),g.removeChild(c),window[j]=void 0},g.appendChild(c)},script:function(b){var c,d=this,e=document.querySelector("head")||document.querySelector("body"),f=a.sync!==!0;if(!e)throw new Error("Ok, wait a second, you want to load a script, but you don't have at least a head or body tag...");c=document.createElement("script"),c.async=f,c.src=b,c.onerror=function(){d.trigger("error",arguments),e.removeChild(c)},c.onload=function(){d.trigger("success",arguments)},e.appendChild(c)}},h=function(b,c,e,f){if("undefined"!=typeof c){if("function"==typeof e)try{c=e.call(d,c)}catch(g){throw new TypeError("Failed to set "+b+" : "+g.message)}return"function"==typeof f?a[b]=f.call(this,c):a[b]=c,this}return"undefined"===a[b]?null:a[b]},i=function(){return["delete","patch","post","put"].indexOf(a.method)>-1},j=function(){var b=a.url,c="undefined"!=typeof a.cache?!!a.cache:!0,d=a.queryString||"",f=a.data;return c===!1&&(d+="&ajabuster="+(new Date).getTime()),b=e(b,d),f&&!i()&&(b=e(b,f)),b};return c},d={bool:function(a){return!!a},string:function(a){if("string"!=typeof a)throw new TypeError("a string is expected, but "+a+" ["+typeof a+"] given");return a},positiveInteger:function(a){if(parseInt(a)!==a||0>=a)throw new TypeError("an integer is expected, but "+a+" ["+typeof a+"] given");return a},plainObject:function(a){if("object"!=typeof a||a.constructor!==Object)throw new TypeError("an object is expected, but "+a+" ["+typeof a+"] given");return a},type:function(b){if(b=this.string(b),a.indexOf(b.toLowerCase())<0)throw new TypeError("a type in ["+a.join(", ")+"] is expected, but "+b+" given");return b.toLowerCase()},method:function(a){if(a=this.string(a),b.indexOf(a.toLowerCase())<0)throw new TypeError("a method in ["+b.join(", ")+"] is expected, but "+a+" given");return a.toLowerCase()},queryString:function(a){var b={};return"string"==typeof a?a.replace("?","").split("&").forEach(function(a){var c=a.split("=");2===c.length&&(b[decodeURIComponent(c[0])]=decodeURIComponent(c[1]))}):b=a,this.plainObject(b)},selector:function(a){if("string"!=typeof a&&!(a instanceof HTMLElement))throw new TypeError("a selector or an HTMLElement is expected, "+a+" ["+typeof a+"] given");return a},func:function(a){if(a=this.string(a),!/^([a-zA-Z_])([a-zA-Z0-9_\-])+$/.test(a))throw new TypeError("a valid function name is expected, "+a+" ["+typeof a+"] given");return a}},e=function(a,b){var c;if(a=a||"",b)if(-1===a.indexOf("?")&&(a+="?"),"string"==typeof b)a+=b;else if("object"==typeof b)for(c in b)a+="&"+encodeURIComponent(c)+"="+encodeURIComponent(b[c]);return a};"function"==typeof define&&define.amd?define([],function(){return c}):"object"==typeof exports?module.exports=c:window.aja=window.aja||c}();
!function(){"use strict";function t(e,n,i){return n=void 0===n?1:n,i=i||n+1,i-n<=1?function(){if(arguments.length<=n||"string"===r.type(arguments[n]))return e.apply(this,arguments);var t,i=arguments[n];for(var o in i){var s=Array.prototype.slice.call(arguments);s.splice(n,1,o,i[o]),t=e.apply(this,s)}return t}:t(t(e,n+1,i),n,i-1)}function e(t,r,i){var o=n(i);if("string"===o){var s=Object.getOwnPropertyDescriptor(r,i);!s||s.writable&&s.configurable&&s.enumerable&&!s.get&&!s.set?t[i]=r[i]:(delete t[i],Object.defineProperty(t,i,s))}else if("array"===o)i.forEach(function(n){n in r&&e(t,r,n)});else for(var a in r)i&&("regexp"===o&&!i.test(a)||"function"===o&&!i.call(r,a))||e(t,r,a);return t}function n(t){if(null===t)return"null";if(void 0===t)return"undefined";var e=(Object.prototype.toString.call(t).match(/^\[object\s+(.*?)\]$/)[1]||"").toLowerCase();return"number"==e&&isNaN(t)?"nan":e}var r=self.Bliss=e(function(t,e){return 2==arguments.length&&!e||!t?null:"string"===r.type(t)?(e||document).querySelector(t):t||null},self.Bliss);e(r,{extend:e,overload:t,type:n,property:r.property||"_",listeners:self.WeakMap?new WeakMap:new Map,original:{addEventListener:(self.EventTarget||Node).prototype.addEventListener,removeEventListener:(self.EventTarget||Node).prototype.removeEventListener},sources:{},noop:function(){},$:function(t,e){return t instanceof Node||t instanceof Window?[t]:2!=arguments.length||e?Array.prototype.slice.call("string"==typeof t?(e||document).querySelectorAll(t):t||[]):[]},defined:function(){for(var t=0;t<arguments.length;t++)if(void 0!==arguments[t])return arguments[t]},create:function(t,e){return t instanceof Node?r.set(t,e):(1===arguments.length&&("string"===r.type(t)?e={}:(e=t,t=e.tag,e=r.extend({},e,function(t){return"tag"!==t}))),r.set(document.createElement(t||"div"),e))},each:function(t,e,n){n=n||{};for(var r in t)n[r]=e.call(t,r,t[r]);return n},ready:function(t,e,n){if("function"!=typeof t||e||(e=t,t=void 0),t=t||document,e&&("loading"!==t.readyState?e():r.once(t,"DOMContentLoaded",function(){e()})),!n)return new Promise(function(e){r.ready(t,e,!0)})},Class:function(t){var e,n=["constructor","extends","abstract","static"].concat(Object.keys(r.classProps)),i=t.hasOwnProperty("constructor")?t.constructor:r.noop;2==arguments.length?(e=arguments[0],t=arguments[1]):(e=function(){if(this.constructor.__abstract&&this.constructor===e)throw new Error("Abstract classes cannot be directly instantiated.");e["super"]&&e["super"].apply(this,arguments),i.apply(this,arguments)},e["super"]=t["extends"]||null,e.prototype=r.extend(Object.create(e["super"]?e["super"].prototype:Object),{constructor:e}),e.prototype["super"]=e["super"]?e["super"].prototype:null,e.__abstract=!!t["abstract"]);var o=function(t){return this.hasOwnProperty(t)&&n.indexOf(t)===-1};if(t["static"]){r.extend(e,t["static"],o);for(var s in r.classProps)s in t["static"]&&r.classProps[s](e,t["static"][s])}r.extend(e.prototype,t,o);for(var s in r.classProps)s in t&&r.classProps[s](e.prototype,t[s]);return e},classProps:{lazy:t(function(t,e,n){return Object.defineProperty(t,e,{get:function(){var t=n.call(this);return Object.defineProperty(this,e,{value:t,configurable:!0,enumerable:!0,writable:!0}),t},set:function(t){Object.defineProperty(this,e,{value:t,configurable:!0,enumerable:!0,writable:!0})},configurable:!0,enumerable:!0}),t}),live:t(function(t,e,n){return"function"===r.type(n)&&(n={set:n}),Object.defineProperty(t,e,{get:function(){var t=this["_"+e],r=n.get&&n.get.call(this,t);return void 0!==r?r:t},set:function(t){var r=this["_"+e],i=n.set&&n.set.call(this,t,r);this["_"+e]=void 0!==i?i:t},configurable:n.configurable,enumerable:n.enumerable}),t})},include:function(){var t=arguments[arguments.length-1],e=2===arguments.length&&arguments[0],n=document.createElement("script");return e?Promise.resolve():new Promise(function(e,i){r.set(n,{async:!0,onload:function(){e(),n.parentNode&&n.parentNode.removeChild(n)},onerror:function(){i()},src:t,inside:document.head})})},fetch:function(t,n){if(!t)throw new TypeError("URL parameter is mandatory and cannot be "+t);var i=e({url:new URL(t,location),data:"",method:"GET",headers:{},xhr:new XMLHttpRequest},n);i.method=i.method.toUpperCase(),r.hooks.run("fetch-args",i),"GET"===i.method&&i.data&&(i.url.search+=i.data),document.body.setAttribute("data-loading",i.url),i.xhr.open(i.method,i.url.href,i.async!==!1,i.user,i.password);for(var o in n)if("upload"===o)i.xhr.upload&&"object"==typeof n[o]&&r.extend(i.xhr.upload,n[o]);else if(o in i.xhr)try{i.xhr[o]=n[o]}catch(s){self.console&&console.error(s)}var a=Object.keys(i.headers).map(function(t){return t.toLowerCase()});"GET"!==i.method&&a.indexOf("content-type")===-1&&i.xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");for(var c in i.headers)void 0!==i.headers[c]&&i.xhr.setRequestHeader(c,i.headers[c]);var u=new Promise(function(t,e){i.xhr.onload=function(){document.body.removeAttribute("data-loading"),0===i.xhr.status||i.xhr.status>=200&&i.xhr.status<300||304===i.xhr.status?t(i.xhr):e(r.extend(Error(i.xhr.statusText),{xhr:i.xhr,get status(){return this.xhr.status}}))},i.xhr.onerror=function(){document.body.removeAttribute("data-loading"),e(r.extend(Error("Network Error"),{xhr:i.xhr}))},i.xhr.ontimeout=function(){document.body.removeAttribute("data-loading"),e(r.extend(Error("Network Timeout"),{xhr:i.xhr}))},i.xhr.send("GET"===i.method?null:i.data)});return u.xhr=i.xhr,u},value:function(t){var e="string"!==r.type(t);return r.$(arguments).slice(+e).reduce(function(t,e){return t&&t[e]},e?t:self)}}),r.Hooks=new r.Class({add:function(t,e,n){if("string"==typeof arguments[0])(Array.isArray(t)?t:[t]).forEach(function(t){this[t]=this[t]||[],e&&this[t][n?"unshift":"push"](e)},this);else for(var t in arguments[0])this.add(t,arguments[0][t],arguments[1])},run:function(t,e){this[t]=this[t]||[],this[t].forEach(function(t){t.call(e&&e.context?e.context:e,e)})}}),r.hooks=new r.Hooks;r.property;r.Element=function(t){this.subject=t,this.data={},this.bliss={}},r.Element.prototype={set:t(function(t,e){t in r.setProps?r.setProps[t].call(this,e):t in this?this[t]=e:this.setAttribute(t,e)},0),transition:function(t,e){return e=+e||400,new Promise(function(n,i){if("transition"in this.style){var o=r.extend({},this.style,/^transition(Duration|Property)$/);r.style(this,{transitionDuration:(e||400)+"ms",transitionProperty:Object.keys(t).join(", ")}),r.once(this,"transitionend",function(){clearTimeout(s),r.style(this,o),n(this)});var s=setTimeout(n,e+50,this);r.style(this,t)}else r.style(this,t),n(this)}.bind(this))},fire:function(t,e){var n=document.createEvent("HTMLEvents");return n.initEvent(t,!0,!0),this.dispatchEvent(r.extend(n,e))},bind:t(function(t,e){if(arguments.length>1&&("function"===r.type(e)||e.handleEvent)){var n=e;e="object"===r.type(arguments[2])?arguments[2]:{capture:!!arguments[2]},e.callback=n}var i=r.listeners.get(this)||{};t.trim().split(/\s+/).forEach(function(t){if(t.indexOf(".")>-1){t=t.split(".");var n=t[1];t=t[0]}i[t]=i[t]||[],0===i[t].filter(function(t){return t.callback===e.callback&&t.capture==e.capture}).length&&i[t].push(r.extend({className:n},e)),r.original.addEventListener.call(this,t,e.callback,e)},this),r.listeners.set(this,i)},0),unbind:t(function(t,e){if(e&&("function"===r.type(e)||e.handleEvent)){var n=e;e=arguments[2]}"boolean"==r.type(e)&&(e={capture:e}),e=e||{},e.callback=e.callback||n;var i=r.listeners.get(this);(t||"").trim().split(/\s+/).forEach(function(t){if(t.indexOf(".")>-1){t=t.split(".");var n=t[1];t=t[0]}if(t&&e.callback)return r.original.removeEventListener.call(this,t,e.callback,e.capture);if(i)for(var o in i)if(!t||o===t)for(var s,a=0;s=i[o][a];a++)n&&n!==s.className||e.callback&&e.callback!==s.callback||!!e.capture!=!!s.capture||(i[o].splice(a,1),r.original.removeEventListener.call(this,o,s.callback,s.capture),a--)},this)},0)},r.setProps={style:function(t){for(var e in t)e in this.style?this.style[e]=t[e]:this.style.setProperty(e,t[e])},attributes:function(t){for(var e in t)this.setAttribute(e,t[e])},properties:function(t){r.extend(this,t)},events:function(t){if(1!=arguments.length||!t||!t.addEventListener)return r.bind.apply(this,[this].concat(r.$(arguments)));var e=this;if(r.listeners){var n=r.listeners.get(t);for(var i in n)n[i].forEach(function(t){r.bind(e,i,t.callback,t.capture)})}for(var o in t)0===o.indexOf("on")&&(this[o]=t[o])},once:t(function(t,e){var n=this,i=function(){return r.unbind(n,t,i),e.apply(n,arguments)};r.bind(this,t,i,{once:!0})},0),delegate:t(function(t,e,n){r.bind(this,t,function(t){t.target.closest(e)&&n.call(this,t)})},0,2),contents:function(t){(t||0===t)&&(Array.isArray(t)?t:[t]).forEach(function(t){var e=r.type(t);/^(string|number)$/.test(e)?t=document.createTextNode(t+""):"object"===e&&(t=r.create(t)),t instanceof Node&&this.appendChild(t)},this)},inside:function(t){t&&t.appendChild(this)},before:function(t){t&&t.parentNode.insertBefore(this,t)},after:function(t){t&&t.parentNode.insertBefore(this,t.nextSibling)},start:function(t){t&&t.insertBefore(this,t.firstChild)},around:function(t){t&&t.parentNode&&r.before(this,t),this.appendChild(t)}},r.Array=function(t){this.subject=t},r.Array.prototype={all:function(t){var e=r.$(arguments).slice(1);return this[t].apply(this,e)}},r.add=t(function(t,e,n,i){n=r.extend({$:!0,element:!0,array:!0},n),"function"==r.type(e)&&(!n.element||t in r.Element.prototype&&i||(r.Element.prototype[t]=function(){return this.subject&&r.defined(e.apply(this.subject,arguments),this.subject)}),!n.array||t in r.Array.prototype&&i||(r.Array.prototype[t]=function(){var t=arguments;return this.subject.map(function(n){return n&&r.defined(e.apply(n,t),n)})}),n.$&&(r.sources[t]=r[t]=e,(n.array||n.element)&&(r[t]=function(){var e=[].slice.apply(arguments),i=e.shift(),o=n.array&&Array.isArray(i)?"Array":"Element";return r[o].prototype[t].apply({subject:i},e)})))},0),r.add(r.Array.prototype,{element:!1}),r.add(r.Element.prototype),r.add(r.setProps),r.add(r.classProps,{element:!1,array:!1});var i=document.createElement("_");r.add(r.extend({},HTMLElement.prototype,function(t){return"function"===r.type(i[t])}),null,!0)}(),function(t){"use strict";if(Bliss&&!Bliss.shy){var e=Bliss.property;t.add({clone:function(){var e=this.cloneNode(!0),n=t.$("*",e).concat(e);return t.$("*",this).concat(this).forEach(function(e,r,i){t.events(n[r],e),n[r]._.data=t.extend({},e._.data)}),e}},{array:!1}),Object.defineProperty(Node.prototype,e,{get:function n(){return Object.defineProperty(Node.prototype,e,{get:void 0}),Object.defineProperty(this,e,{value:new t.Element(this)}),Object.defineProperty(Node.prototype,e,{get:n}),this[e]},configurable:!0}),Object.defineProperty(Array.prototype,e,{get:function(){return Object.defineProperty(this,e,{value:new t.Array(this)}),this[e]},configurable:!0}),self.EventTarget&&"addEventListener"in EventTarget.prototype&&(EventTarget.prototype.addEventListener=function(e,n,r){return t.bind(this,e,n,r)},EventTarget.prototype.removeEventListener=function(e,n,r){return t.unbind(this,e,n,r)}),self.$=self.$||t,self.$$=self.$$||t.$}}(Bliss);
(function(root,smoothScroll){"use strict";if(typeof define==="function"&&define.amd){define(smoothScroll)}else if(typeof exports==="object"&&typeof module==="object"){module.exports=smoothScroll()}else{root.smoothScroll=smoothScroll()}})(this,function(){"use strict";if(typeof window!=="object")return;if(document.querySelectorAll===void 0||window.pageYOffset===void 0||history.pushState===void 0){return}var getTop=function(element,start){if(element.nodeName==="HTML")return-start;return element.getBoundingClientRect().top+start};var easeInOutCubic=function(t){return t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1};var position=function(start,end,elapsed,duration){if(elapsed>duration)return end;return start+(end-start)*easeInOutCubic(elapsed/duration)};var smoothScroll=function(el,duration,callback,context){duration=duration||500;context=context||window;var start=context.scrollTop||window.pageYOffset;if(typeof el==="number"){var end=parseInt(el)}else{var end=getTop(el,start)}var clock=Date.now();var requestAnimationFrame=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||function(fn){window.setTimeout(fn,15)};var step=function(){var elapsed=Date.now()-clock;if(context!==window){context.scrollTop=position(start,end,elapsed,duration)}else{window.scroll(0,position(start,end,elapsed,duration))}if(elapsed>duration){if(typeof callback==="function"){callback(el)}}else{requestAnimationFrame(step)}};step()};var linkHandler=function(ev){if(!ev.defaultPrevented){ev.preventDefault();if(location.hash!==this.hash)window.history.pushState(null,null,this.hash);var node=document.getElementById(this.hash.substring(1));if(!node)return;smoothScroll(node,500,function(el){location.replace("#"+el.id)})}};document.addEventListener("DOMContentLoaded",function(){var internal=document.querySelectorAll('a[href^="#"]:not([href="#"])'),a;for(var i=internal.length;a=internal[--i];){a.addEventListener("click",linkHandler,false)}});return smoothScroll});

class Admin {
    constructor() {
        this.admin = (location.search.indexOf('admin') > -1);
        cache.setItem('admin', "true");
        this.messages = [];
        
        this.visibility = this.admin;

        this.toolbar();
    }

    toolbar() {
        let toolbar = $.create('nav', {
            className: "navbar is-fixed-bottom toolbar move-up",
            contents: [
                {
                    tag: "div",
                    className: "navbar-item has-dropdown-up is-hoverable",
                    contents: [
                        this.navbarItem("Tools", {
                            className: "navbar-link",
                        }),
                        this.navbarDropdown([
                            this.navbarItem("Delete Cache", {
                                onclick: () => resetCache()
                            }),
                            this.navbarItem("Reload w/o Admin", {
                                onclick: () => location.href = location.origin
                            }),
                            this.navbarItem("Reload", {
                                onclick: () => location.href = `${location.host}/${location.search}`
                            })
                        ]),
                    ]
                },
                {
                    tag: "div",
                    className: "navbar-item has-dropdown-up is-hoverable",
                    contents: [
                        this.navbarItem("Troubleshooting", {
                            className: "navbar-link",
                        }),
                        this.navbarDropdown([
                            this.navbarItem("Execute scripts", {
                                onclick: () => exec()
                            }),
                            this.navbarItem("Get tree", {
                                onclick: () => tree()
                            }),
                        ]),
                    ]
                },
                {
                    tag: "div",
                    className: "navbar-end",
                    contents: [
                        this.navbarItem("Troubleshooting", {
                            tag: "p",
                            className: "navbar-item message",
                            textContent: "",
                        })
                    ]
                },
            ]
        });

        if (this.visibility === true) {
            document.body.appendChild(toolbar);
            document.documentElement.classList.add('has-navbar-fixed-bottom');
        } else {
            if ($('.toolbar') !== null) {
                $('.toolbar').remove();
                document.documentElement.classList.remove('has-navbar-fixed-bottom');
            }
        }
    }

    updateMessage(msg, type = "light") {
        let timestamp = new Date();
        let message = $('.toolbar .message');
        if (message) {
            message.textContent = `${timestamp.toLocaleTimeString()} - ${msg}`;
            message.classList.add(type);
            this.messages.push(`${timestamp.toLocaleTimeString()} - ${msg}`);
        }
    }

    navbarDropdown(contents) {
        return {
            tag: "div",
            className: "navbar-dropdown",
            contents: contents
        }
    }

    navbarItem(textContent, options) {
        return Object.assign({
            tag: "a",
            className: "navbar-item",
            textContent: textContent
        }, options);
    }
}

/**
 * Manages frames display to avoid data-redownload and increases overall speed
 * */
class Frames {
    constructor() {
        this.frames = {};
        this.getContainer();
    }

    getContainer() {
        this.container = document.getElementById('main');
    }

    snapshot() {
        this.getContainer();
        let name = location.hash.replace(/[#]/g, "");
        let fragment = new DocumentFragment();

        if (name === "") name = "index";

        fragment.appendChild(this.container.cloneNode(true));

        this.frames[name] = fragment;
    }

    retrieve(name) {
        if (this.frames[name] !== undefined) {
            return this.frames[name].firstChild.innerHTML;
        }

        return false;
    }

    contain(name) {
        if (this.frames[name] !== undefined) {
			let frag = this.frames[name].querySelector('section').cloneNode(true);
			let screen = document.createElement('div');
			screen.cssText = "position:absolute; top:0; left:0;z-index:10";
			screen.appendChild(frag);

			this.container.insertBefore(screen, this.container.firstChild);
        }
    }
}
class Parse {
    encode(text) {
        let chars = [];
        let mapping = {
            "é": "&eacute;",
            "è": "&egrave;",
            "ë": "&euml;",
            "ê": "&ecirc;",
            "à": "&agrave;",
            "á": "&aacute;",
            "â": "&acirc;",
            "ç": "&ccedil;",
            "ù": "&ugrave;",
            "ú": "&uacute;",
            "û": "&ucirc;",
            "ü": "&uuml;",
            "ò": "&ograve;",
            "õ": "&otilde;",
            "ó": "&oacute;",
            "ô": "&ocirc;",
            "ö": "&ouml;",
            "ì": "&igrave;",
            "í": "&iacute;",
            "ï": "&iuml;",
            "î": "&icirc;",
        };

        for (let char in mapping) {
            chars.push(char);
        }

        let pattern = `[${chars.join('')}]`;
        let reg = new RegExp(pattern, 'gi');

        return text.replace(reg, e => {
            let output = mapping[e.toLowerCase()];
            if (this.isUpperCase(e)) output = output.replace(output[1], output[1].toUpperCase());
            return output;
        });
    }

    isUpperCase(letter) {
        return letter === letter.toUpperCase();
    }
};
// Tooling
const cache = window.localStorage;
const frames = new Frames();
const loading = document.getElementById('loading');

// Helper to resolve components
const components = (file) => `/static/components/${file}.html`;

/**
 * Polyfill to enable forEach on nodeLists
 * */
(function () {
    if (typeof NodeList.prototype.forEach === "function") return false;
    NodeList.prototype.forEach = Array.prototype.forEach;
})();


/**
 * Load html templates
 * */
$.include(window.Card, "./static/components/blocks/card.js");

/**
 * Reads the hash, parses it and returns obtains result. If empty, sets to index.
 * Is used as an event handler.
 * To enable anchor-functionality within document, prepend your URL with a "!"
 *
 * @return {Array}
 */
const hash = () => {
    // "!" in hash allows to override the router and set anchors in page
    loading.style.display = '';
    if (location.hash.indexOf("!") === -1) {
        let hash = location.hash.replace(/[#]/ig, "").split("/");

        if (hash[0] === "") {
            return ["index"];
        }

        return hash;
    }

    return null;
};

const admin = new Admin();

/**
 * Wrapper around grab
 * */
const router = () => {
    return grab(hash());
};

/**
 * Given a page name, go grab it in either cache or fetch it if not present
 *
 * @param {String} page : the page name
 * @param {HTMLElement} container : where it should be appended
 * @return void
 * */
function grab(page, container = undefined) {
    if (container === undefined) container = document.querySelector("#main");
    if (page === null) return;
    let file = page.join("/");
    let fragment = frames.retrieve(file);

    if (fragment !== false) {
        let clone = container.cloneNode(false);
        container.parentNode.replaceChild(clone, container);
        clone.innerHTML = fragment;
        tree();
        exec();
        loading.style.display = 'none';

    } else {
        aja()
            .url(components(file))
            .type('html')
            .on('success', (data) => {
                // cleaning up:
                let clone = container.cloneNode(false);
                container.parentNode.replaceChild(clone, container);
                clone.innerHTML = data;
                frames.snapshot();
                tree();
                exec();
                loading.style.display = 'none';
            })
            .go();
    }
}

/**
 * tree() parses all files and looks for special elements which have the
 * data-template property, and uses its value to fetch the fragment and replace
 * the data-template with the fragment.
 *
 * It is called on every fragment to allow fragment nesting
 *
 * @param {HTMLElement} el
 * @return {Function} appendr
 * */
function tree(el) {
    let templates = document.querySelectorAll('[data-template]');

    if (el !== undefined) {
        templates = el.querySelectorAll(`[data-template]`);
    }

    if (templates.length > 0) {
        templates.forEach((t) => {
            let template = t.dataset.template;

            if (cache.getItem(template) === null) {
                return aja()
                    .url(components(`templates/${template}`))
                    .type('html')
                    .on('success', (r) => {
                        appendr(r, template);

                        cache.setItem(template, r.replace(/(?:\s{2,})+/g, "").trim());
                        // exec();
                    })
                    .go();
            }

            return appendr(cache.getItem(template), template);
        });
    }
}

/**
 * Executes all scripts tags which have no source defined
 *
 * @param {HTMLElement} el: optionnal, allows executing on some specific element. Defaults to document.
 * @return void
 * */
async function exec(el) {
    if (!el) {
        el = document;
    }

    el.querySelectorAll('script:not([src])').forEach((script) => {
        eval(script.innerHTML);
    });
}

/**
 * Inserts html in data-template element, replace data-template element in DOM
 *
 * @param {string} html
 * @param {string} template
 * @return void
 * */
async function appendr(html, template) {
    let df = document.createRange().createContextualFragment(html);
    tree(df);
    document.querySelector(`[data-template="${template}"]`).outerHTML = html;
    exec(df);
    setActive();
}

/**
 * Parses templates and replaces ${} placeholders with their values from json param
 *
 * @param {Object} json
 * @param {HTMLElement} block
 * @return {HTMLElement} template
 * */
function replacr(json, block) {
    if (json !== undefined && block !== undefined) {

        let parent = block.parentElement || document.querySelector(`.${block.className.replace(/[ ]/g, ".")}`).parentElement;
        let template = block.cloneNode(true);
        let blocks = [];

        json.forEach((j) => {
            block = template.cloneNode(true);
            let html = block.innerHTML;

            html = html.replace(/\${\w+}/ig, (v) => {
                let r = v.replace(/[$\{\}]/g, "");

                // if its an array (multiple phone numbers, just stringify them with a slash between each
                if (j[r] instanceof Array) {
                    return j[r].join(' / ')
                }
                return j[r];
            });

            block.innerHTML = html;
            block.classList.remove('is-hidden');

            blocks.push(block);
        });

        if (parent !== null) {
            let df = new DocumentFragment();

            df.appendChild(template.cloneNode(true));
            blocks.forEach((b) => {
                df.appendChild(b);
            });

            parent.innerHTML = '';
            parent.appendChild(df);
        }

        return template;
    }
}

/**
 * Utilities.
 * */

/**
 * Encode accented characters to HTML entity
 * */
entitify = (str) => {
    return str.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
};

/**
 * Efficiently removes child nodes with event listeners' references
 * */
function clean(node) {
    let cNode = node.cloneNode(false);
    return node.parentNode.replaceChild(cNode, node);
}

/**
 * Formats a string of numbers to the Swiss format. Outputs to an array with each capture
 *
 * @param {string} phoneNb
 * @return {Array} phone
 * */
function swissPhoneFormat(phoneNb) {
    // parses 0041216137385 / 0781234567
    let reg = /^(0041|041|\+41|\+\+41|41)?(\d{2}|\d{3})(\d{3})(\d{2})(\d{2})$/;
    if (!phoneNb.match(reg)) return [];
    let phone = phoneNb.match(reg).slice(1).filter((v) => (v !== undefined));

    return phone;
}

/**
 * Visual helper to highlight in the menu on which page we're on
 *
 * @return void
 * */
async function setActive() {
    let menu = document.querySelector(`#navbarMenuHeroC .is-active`);
    menu.classList.remove('is-active');
    let hash = location.hash === "" ? "#" : location.hash;
    menu = document.querySelector(`#navbarMenuHeroC [href="${hash}"]`);
    menu.classList.add('is-active');
}

function shutDown() {

    let ask = prompt("Ceci va interrompre l'application. Continuer?");
    console.log(ask);
    aja()
        .url('/shutdown')
        .method('get')
        .go();

    return "Ceci va interrompre l'application. Êtes-vous sûr?";
}

window.addEventListener('beforeunload', shutDown);
window.onunload = shutDown;
window.onbeforeunload = shutDown;

/**
 * A refresh to set everything anew when reloading the website
 *
 * @return void
 * */
function resetCache() {
    for (let key in localStorage) {
        localStorage.removeItem(key);
    }
}

/**
 * Purge the cache on each app loading - better to start with a fresh mind!
 * */
resetCache();

/**
 * Enabling the router
 * */
window.addEventListener('hashchange', () => {
    router();
});

router();

/**
 * Is the app started freshly?
 * */
aja()
    .url("/api/setup/")
    .method("get")
    .type("json")
    .on("success", (resp) => {
        if (resp.initiated === false) {
            setTimeout(() => location.hash = `profiles`, 500);
        }
    })
    .go();
