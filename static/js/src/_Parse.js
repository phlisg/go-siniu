class Parse {
    encode(text) {
        let chars = [];
        let mapping = {
            "é": "&eacute;",
            "è": "&egrave;",
            "ë": "&euml;",
            "ê": "&ecirc;",
            "à": "&agrave;",
            "á": "&aacute;",
            "â": "&acirc;",
            "ç": "&ccedil;",
            "ù": "&ugrave;",
            "ú": "&uacute;",
            "û": "&ucirc;",
            "ü": "&uuml;",
            "ò": "&ograve;",
            "õ": "&otilde;",
            "ó": "&oacute;",
            "ô": "&ocirc;",
            "ö": "&ouml;",
            "ì": "&igrave;",
            "í": "&iacute;",
            "ï": "&iuml;",
            "î": "&icirc;",
        };

        for (let char in mapping) {
            chars.push(char);
        }

        let pattern = `[${chars.join('')}]`;
        let reg = new RegExp(pattern, 'gi');

        return text.replace(reg, e => {
            let output = mapping[e.toLowerCase()];
            if (this.isUpperCase(e)) output = output.replace(output[1], output[1].toUpperCase());
            return output;
        });
    }

    isUpperCase(letter) {
        return letter === letter.toUpperCase();
    }
};