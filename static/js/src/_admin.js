class Admin {
    constructor() {
        this.admin = (location.search.indexOf('admin') > -1);
        cache.setItem('admin', "true");
        this.messages = [];
        
        this.visibility = this.admin;

        this.toolbar();
    }

    toolbar() {
        let toolbar = $.create('nav', {
            className: "navbar is-fixed-bottom toolbar move-up",
            contents: [
                {
                    tag: "div",
                    className: "navbar-item has-dropdown-up is-hoverable",
                    contents: [
                        this.navbarItem("Tools", {
                            className: "navbar-link",
                        }),
                        this.navbarDropdown([
                            this.navbarItem("Delete Cache", {
                                onclick: () => resetCache()
                            }),
                            this.navbarItem("Reload w/o Admin", {
                                onclick: () => location.href = location.origin
                            }),
                            this.navbarItem("Reload", {
                                onclick: () => location.href = `${location.host}/${location.search}`
                            })
                        ]),
                    ]
                },
                {
                    tag: "div",
                    className: "navbar-item has-dropdown-up is-hoverable",
                    contents: [
                        this.navbarItem("Troubleshooting", {
                            className: "navbar-link",
                        }),
                        this.navbarDropdown([
                            this.navbarItem("Execute scripts", {
                                onclick: () => exec()
                            }),
                            this.navbarItem("Get tree", {
                                onclick: () => tree()
                            }),
                        ]),
                    ]
                },
                {
                    tag: "div",
                    className: "navbar-end",
                    contents: [
                        this.navbarItem("Troubleshooting", {
                            tag: "p",
                            className: "navbar-item message",
                            textContent: "",
                        })
                    ]
                },
            ]
        });

        if (this.visibility === true) {
            document.body.appendChild(toolbar);
            document.documentElement.classList.add('has-navbar-fixed-bottom');
        } else {
            if ($('.toolbar') !== null) {
                $('.toolbar').remove();
                document.documentElement.classList.remove('has-navbar-fixed-bottom');
            }
        }
    }

    updateMessage(msg, type = "light") {
        let timestamp = new Date();
        let message = $('.toolbar .message');
        if (message) {
            message.textContent = `${timestamp.toLocaleTimeString()} - ${msg}`;
            message.classList.add(type);
            this.messages.push(`${timestamp.toLocaleTimeString()} - ${msg}`);
        }
    }

    navbarDropdown(contents) {
        return {
            tag: "div",
            className: "navbar-dropdown",
            contents: contents
        }
    }

    navbarItem(textContent, options) {
        return Object.assign({
            tag: "a",
            className: "navbar-item",
            textContent: textContent
        }, options);
    }
}
