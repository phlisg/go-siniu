// Tooling
const cache = window.localStorage;
const frames = new Frames();
const loading = document.getElementById('loading');

// Helper to resolve components
const components = (file) => `/static/components/${file}.html`;

/**
 * Polyfill to enable forEach on nodeLists
 * */
(function () {
    if (typeof NodeList.prototype.forEach === "function") return false;
    NodeList.prototype.forEach = Array.prototype.forEach;
})();


/**
 * Load html templates
 * */
$.include(window.Card, "./static/components/blocks/card.js");

/**
 * Reads the hash, parses it and returns obtains result. If empty, sets to index.
 * Is used as an event handler.
 * To enable anchor-functionality within document, prepend your URL with a "!"
 *
 * @return {Array}
 */
const hash = () => {
    // "!" in hash allows to override the router and set anchors in page
    loading.style.display = '';
    if (location.hash.indexOf("!") === -1) {
        let hash = location.hash.replace(/[#]/ig, "").split("/");

        if (hash[0] === "") {
            return ["index"];
        }

        return hash;
    }

    return null;
};

const admin = new Admin();

/**
 * Wrapper around grab
 * */
const router = () => {
    return grab(hash());
};

/**
 * Given a page name, go grab it in either cache or fetch it if not present
 *
 * @param {String} page : the page name
 * @param {HTMLElement} container : where it should be appended
 * @return void
 * */
function grab(page, container = undefined) {
    if (container === undefined) container = document.querySelector("#main");
    if (page === null) return;
    let file = page.join("/");
    let fragment = frames.retrieve(file);

    if (fragment !== false) {
        let clone = container.cloneNode(false);
        container.parentNode.replaceChild(clone, container);
        clone.innerHTML = fragment;
        tree();
        exec();
        loading.style.display = 'none';

    } else {
        aja()
            .url(components(file))
            .type('html')
            .on('success', (data) => {
                // cleaning up:
                let clone = container.cloneNode(false);
                container.parentNode.replaceChild(clone, container);
                clone.innerHTML = data;
                frames.snapshot();
                tree();
                exec();
                loading.style.display = 'none';
            })
            .go();
    }
}

/**
 * tree() parses all files and looks for special elements which have the
 * data-template property, and uses its value to fetch the fragment and replace
 * the data-template with the fragment.
 *
 * It is called on every fragment to allow fragment nesting
 *
 * @param {HTMLElement} el
 * @return {Function} appendr
 * */
function tree(el) {
    let templates = document.querySelectorAll('[data-template]');

    if (el !== undefined) {
        templates = el.querySelectorAll(`[data-template]`);
    }

    if (templates.length > 0) {
        templates.forEach((t) => {
            let template = t.dataset.template;

            if (cache.getItem(template) === null) {
                return aja()
                    .url(components(`templates/${template}`))
                    .type('html')
                    .on('success', (r) => {
                        appendr(r, template);

                        cache.setItem(template, r.replace(/(?:\s{2,})+/g, "").trim());
                        // exec();
                    })
                    .go();
            }

            return appendr(cache.getItem(template), template);
        });
    }
}

/**
 * Executes all scripts tags which have no source defined
 *
 * @param {HTMLElement} el: optionnal, allows executing on some specific element. Defaults to document.
 * @return void
 * */
async function exec(el) {
    if (!el) {
        el = document;
    }

    el.querySelectorAll('script:not([src])').forEach((script) => {
        eval(script.innerHTML);
    });
}

/**
 * Inserts html in data-template element, replace data-template element in DOM
 *
 * @param {string} html
 * @param {string} template
 * @return void
 * */
async function appendr(html, template) {
    let df = document.createRange().createContextualFragment(html);
    tree(df);
    document.querySelector(`[data-template="${template}"]`).outerHTML = html;
    exec(df);
    setActive();
}

/**
 * Parses templates and replaces ${} placeholders with their values from json param
 *
 * @param {Object} json
 * @param {HTMLElement} block
 * @return {HTMLElement} template
 * */
function replacr(json, block) {
    if (json !== undefined && block !== undefined) {

        let parent = block.parentElement || document.querySelector(`.${block.className.replace(/[ ]/g, ".")}`).parentElement;
        let template = block.cloneNode(true);
        let blocks = [];

        json.forEach((j) => {
            block = template.cloneNode(true);
            let html = block.innerHTML;

            html = html.replace(/\${\w+}/ig, (v) => {
                let r = v.replace(/[$\{\}]/g, "");

                // if its an array (multiple phone numbers, just stringify them with a slash between each
                if (j[r] instanceof Array) {
                    return j[r].join(' / ')
                }
                return j[r];
            });

            block.innerHTML = html;
            block.classList.remove('is-hidden');

            blocks.push(block);
        });

        if (parent !== null) {
            let df = new DocumentFragment();

            df.appendChild(template.cloneNode(true));
            blocks.forEach((b) => {
                df.appendChild(b);
            });

            parent.innerHTML = '';
            parent.appendChild(df);
        }

        return template;
    }
}

/**
 * Utilities.
 * */

/**
 * Encode accented characters to HTML entity
 * */
entitify = (str) => {
    return str.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
};

/**
 * Efficiently removes child nodes with event listeners' references
 * */
function clean(node) {
    let cNode = node.cloneNode(false);
    return node.parentNode.replaceChild(cNode, node);
}

/**
 * Formats a string of numbers to the Swiss format. Outputs to an array with each capture
 *
 * @param {string} phoneNb
 * @return {Array} phone
 * */
function swissPhoneFormat(phoneNb) {
    // parses 0041216137385 / 0781234567
    let reg = /^(0041|041|\+41|\+\+41|41)?(\d{2}|\d{3})(\d{3})(\d{2})(\d{2})$/;
    if (!phoneNb.match(reg)) return [];
    let phone = phoneNb.match(reg).slice(1).filter((v) => (v !== undefined));

    return phone;
}

/**
 * Visual helper to highlight in the menu on which page we're on
 *
 * @return void
 * */
async function setActive() {
    let menu = document.querySelector(`#navbarMenuHeroC .is-active`);
    menu.classList.remove('is-active');
    let hash = location.hash === "" ? "#" : location.hash;
    menu = document.querySelector(`#navbarMenuHeroC [href="${hash}"]`);
    menu.classList.add('is-active');
}

function shutDown() {
    aja()
        .url('/shutdown')
        .method('get')
        .go();

    return "Ceci va interrompre l'application. Êtes-vous sûr?";
}

window.addEventListener('beforeunload', shutDown);
window.onunload = shutDown;
window.onbeforeunload = shutDown;

/**
 * A refresh to set everything anew when reloading the website
 *
 * @return void
 * */
function resetCache() {
    for (let key in localStorage) {
        localStorage.removeItem(key);
    }
}

/**
 * Purge the cache on each app loading - better to start with a fresh mind!
 * */
resetCache();

/**
 * Enabling the router
 * */
window.addEventListener('hashchange', () => {
    router();
});

router();

/**
 * Is the app started freshly?
 * */
aja()
    .url("/api/setup/")
    .method("get")
    .type("json")
    .on("success", (resp) => {
        if (resp.initiated === false) {
            setTimeout(() => location.hash = `profiles`, 500);
        }
    })
    .go();
