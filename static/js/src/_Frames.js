/**
 * Manages frames display to avoid data-redownload and increases overall speed
 * */
class Frames {
    constructor() {
        this.frames = {};
        this.getContainer();
    }

    getContainer() {
        this.container = document.getElementById('main');
    }

    snapshot() {
        this.getContainer();
        let name = location.hash.replace(/[#]/g, "");
        let fragment = new DocumentFragment();

        if (name === "") name = "index";

        fragment.appendChild(this.container.cloneNode(true));

        this.frames[name] = fragment;
    }

    retrieve(name) {
        if (this.frames[name] !== undefined) {
            return this.frames[name].firstChild.innerHTML;
        }

        return false;
    }

    contain(name) {
        if (this.frames[name] !== undefined) {
			let frag = this.frames[name].querySelector('section').cloneNode(true);
			let screen = document.createElement('div');
			screen.cssText = "position:absolute; top:0; left:0;z-index:10";
			screen.appendChild(frag);

			this.container.insertBefore(screen, this.container.firstChild);
        }
    }
}