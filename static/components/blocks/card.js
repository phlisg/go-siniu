const Card = function () {
    let df = new DocumentFragment;
    let div = document.createElement('div');
    div.innerHTML = `<div class="card">
    <div class="card-image section">
        <div class="disabled"></div>
        <div data-placeholder="signature"></div>
    </div>
    <div class="card-content">
        <div class="content" data-placeholder="content"></div>
    </div>
    <footer data-placeholder="buttons" class="card-footer">
        <a data-action="install" class="card-footer-item has-text-white">Install/Remove</a>
        <a data-action="edit" class="card-footer-item" disabled>Éditer</a>
        <a data-action="delete" class="card-footer-item">Supprimer</a>
  </footer>
</div>`;

    let card = df.appendChild(div).cloneNode(true);

    let placeholders = () => {
        let p = {};
        let placeholders = card.querySelectorAll('[data-placeholder]');
        placeholders.forEach((placeholder) => {
            p[placeholder.dataset.placeholder] = placeholder;
        });

        return p;
    }

    /**
     * Finds placeholder and replaces its content with parameter element's content
     * Refreshes Card object with newer html
     * @param {HTMLElement} placeholder
     * */
    let insert = (placeholder) => {
        let selector = placeholder.dataset.placeholder;
        card.querySelector(`[data-placeholder="${selector}"]`).innerHTML = placeholder.innerHTML;
        return this;
    }

    return {
        render: () => card,
        placeholders: placeholders(),
        insert: insert,
    };
}