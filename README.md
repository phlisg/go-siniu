#Siniu (means "to sign" in some language)
*(Actually forgot from which language...)*
### A Golang / JS app to manage your email signatures

This project is an experiment which uses Go as a local web server and as an API provider for Javascript, to create and manage responsive email signatures.
The app runs in the browser but also provides system access to read and write files.

In the end, I was lazy to learn and use Electron (plus the fact it is very Ram-heavy because of chromium) and made a sort of mini "GoLectron", running JS apps in the browser (well actually, it's not so different than Electron...).

The project is currently designed for Windows and for Outlook 2013+ email client.

For personal knowledge and learning, this project does not use any fancy and well-known frameworks, but instead relies on small librairies to add some custom functionnality, such as [aja.js](http://krampstudio.com/aja.js/) as a tiny Ajax library and [Blissjs](http://blissfuljs.com/) as a light, jQuery-contender in creating and managing virtual nodes.

JS is written in a sort-of ES6 version (did not install a transpiler and I run the code directly in a major browser).

### How it works
Go is used here as an API (learning REST on the way) and provides the main entry to JS through the main layout. From there, a classic big JS file is read and the "app" is launched.

*(The use of Go here is not so interesting, since I'm still learning it)*

Currently, there are a few main functions allowing the whole thing to work:

- `router()`: reads the URL bar, parses the segments and sends them to `grab()`
- `grab()`: retrieves the file either in the "virtual" cache or fetches it via ajax if not present, then stores it in cache 
- `tree()` [to be renamed]: allows building "components-like" html files: create an element with the `data-template="filename"` property (e.g: `<div data-template="navbar"></div>`), and it will go and look for the file in a given folder (currently: `web/components/blocks`), and feed the file to `appendr()`. Plus, that component gets stored in the cache.
- `exec()`: in addition to "components" support, each "component" can be a mix of html and javascript, embedded in `<script>` tags. Those `script` tags are executed after each retrieval of any file in the cache, in a DocumentFragment or after an Ajax call. This uses the evil `eval()`, but hey, why not? :-) *(needs to be optimized as `exec()` is called multiple times during a request)*
- `replacr()`: provides dynamic variable support in static files, through the `${}` syntax.
- `appendr()`: Replaces the `data-template` element's outerHTML (yes, not such a great idea for perfs) with the retrieved file.

### Current features
- Create signature templates through a JS file using [Blissjs](http://blissfuljs.com/)
- Create and manage multiple profiles
- Use of a simple cache system
- Flat-file, uses static files to store data

### Roadmap
- Improve coding style
- Implement tests (yep, shame on me)
- Simplify/further develop some stuff (routing, caching...)
- Install and watch out for signature integrity in Windows' default signature folder
- Implement signature encoding to an `.htm` file 
- Implement signature editing/deletion
- Implement file drop support to add banners to signature
- Add comments everywhere 
- Support template signature editing via GUI to change some default values (social network link, and so on...)

### How to create a signature template
A template is currently managed by a single JS file which has two main methods, `renderForm()` and `render()`. The former builds a virtual form asking the user all the information one signature needs, and the latter updates current template with the values from the form.

Both `renderForm()` and `render()` are "in-memory" to allow this "binding" between the form and the signature.

For the `constructor`, describe the main properties the signature should have (social networks for example).

In the template example, there are a few helper methods adopting the `blissjs`-way to create nodes and easing the creation of common nodes in the signature.

An html signature uses very basic HTML, so you might want to build one from some online generator, import it, remove useless stuff and write line by line each node it contains. Yep, a bit redundant, should think of a way to "import" an `html` file and transforming it into a template. Maybe not in JS.